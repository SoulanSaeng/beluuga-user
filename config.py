import os
from dotenv import load_dotenv


def _database_uri():
    host = os.getenv('DATABASE_HOST')
    port = os.getenv('DATABASE_PORT')
    schema = os.getenv('DATABASE_SCHEMA')
    user = os.getenv('DATABASE_USER')
    password = os.getenv('DATABASE_PASSWORD')
    protocol = os.getenv('DATABASE_PROTOCOL')
    return protocol + '://' + user + ':' + password + '@' + host + ':' + port + '/' + schema


def get_config_for_env():
    env = os.getenv("FLASK_ENV")
    if not env:
        raise Exception('FLASK_ENV is not defined!')
    elif env == 'development':
        return DevelopmentConfig
    elif env == 'test':
        return TestConfig
    elif env == 'production':
        return ProductionConfig
    else:
        raise Exception(f'Unknown configuration type {env}')


class Config(object):
    load_dotenv('.env')
    FLASK_ENV = os.getenv("FLASK_ENV")
    FLASK_APP = os.getenv("FLASK_APP")
    MAILGUN_API_KEY = os.getenv("MAILGUN_API_KEY")
    MAILGUN_API_URL = os.getenv("MAILGUN_API_URL")
    JWT_ALGORITHM = os.getenv("JWT_ALGORITHM") or "HS256"
    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY") or "default"
    JWT_BLACKLIST_TOKEN_CHECKS = ["access", "refresh"]
    JWT_SECRET_TOKEN_EXPIRES = os.getenv("JWT_SECRET_TOKEN_EXPIRES") or 10800
    GOOGLE_CLIENT_ID = os.getenv('GOOGLE_CLIENT_ID')
    GOOGLE_CLIENT_SECRET = os.getenv('GOOGLE_CLIENT_SECRET')
    SQLALCHEMY_DATABASE_URI = _database_uri()


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTION = True
    JWT_BLACKLIST_ENABLED = False


class TestConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTION = True
    JWT_BLACKLIST_ENABLED = True


class ProductionConfig(Config):
    DEBUG = False

from config import get_config_for_env
from flask import Flask
from flask_restful import Api


from flask_jwt_extended import JWTManager
from flask_cors import CORS

from beluuga.persist import db
from beluuga.resources.user import UserCreate
from beluuga.resources.auth import Auth
from beluuga.resources.password import Reset
from beluuga.resources.google_login import GoogleAuthorized

import logging


cfg = get_config_for_env()
app = Flask(cfg.FLASK_APP, instance_relative_config=True)
app.config.from_object(cfg)
CORS(app)
JWTManager(app)

db.init_app(app)


gunicorn_handler = logging.getLogger('gunicorn.error')
app.logger.handlers.extend(gunicorn_handler.handlers)

if cfg.FLASK_ENV == 'development':
    app.logger.setLevel(logging.DEBUG)
else:
    app.logger.setLevel(logging.INFO)


api = Api(app)
api.add_resource(Auth, "/auth")
api.add_resource(Reset, "/user/password")
api.add_resource(UserCreate, "/user")
api.add_resource(GoogleAuthorized, "/auth/google/authorized")


@app.before_first_request
def create_tables():
    db.create_all()


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

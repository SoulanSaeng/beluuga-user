BAD_CRED = "Invalid credentials."
BAD_PWD = (
    "The password must at least of length 8, contain numerical and capital character."
)
BAD_F_NAME = "The first name must not be blank"
BAD_L_NAME = "The last name must not be blank"
BAD_EMAIL = "Email should be defined."
FIELD_REQUIRED = "{} is required."
RESOURCE_EXISTS = "{} already exists."
UNKNOWN_ERROR = "Error occurred!"

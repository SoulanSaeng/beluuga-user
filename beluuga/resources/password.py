from flask_restful import Resource, reqparse

from beluuga.service.user import send_reset_pw_mail, update_pwd
from beluuga.exceptions.api import APIException

from beluuga.constants.keys import MSG, OTP, NEW_PWD
from beluuga.constants.errors import FIELD_REQUIRED
from beluuga.constants.columns import EMAIL

reset_parser = reqparse.RequestParser()
reset_parser.add_argument(
    EMAIL, type=str, required=True, help=FIELD_REQUIRED.format(EMAIL)
)
new_pwd_parser = reqparse.RequestParser()
new_pwd_parser.add_argument(EMAIL, type=str, required=True, help=FIELD_REQUIRED.format(EMAIL))
new_pwd_parser.add_argument(NEW_PWD, type=str, required=True, help=FIELD_REQUIRED.format(NEW_PWD))
new_pwd_parser.add_argument(OTP, type=str, required=True, help=FIELD_REQUIRED.format(OTP))


class Reset(Resource):
    @staticmethod
    def put():
        try:
            data = reset_parser.parse_args()
            send_reset_pw_mail(data[EMAIL])
        except APIException as error:
            return {MSG: error.message}, error.http_code
        else:
            return {MSG: "A reset link was sent to the email."}, 200

    @staticmethod
    def post():
        try:
            data = new_pwd_parser.parse_args()
            success = update_pwd(data[EMAIL], data[OTP], data[NEW_PWD])
        except APIException as error:
            return {MSG: error.message}, error.http_code
        else:
            if success:
                return {MSG: "Password was changed successfully."}, 200
            else:
                return {MSG: "Token provided is either expired or wrong."}, 401

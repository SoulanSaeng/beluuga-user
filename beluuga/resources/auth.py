from flask_restful import Resource, reqparse
from beluuga.service.user import get_user
from beluuga.security.encrypt import Encrypt
from beluuga.exceptions.api import APIException

from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_refresh_token_required,
    get_jwt_identity,
)

from beluuga.constants.errors import FIELD_REQUIRED, BAD_CRED
from beluuga.constants.keys import MSG, TOKEN, R_TOKEN
from beluuga.constants.columns import EMAIL, PWD


login_parser = reqparse.RequestParser()
login_parser.add_argument(
    EMAIL, type=str, required=True, help=FIELD_REQUIRED.format(EMAIL)
)
login_parser.add_argument(PWD, type=str, required=True, help=FIELD_REQUIRED.format(PWD))


class Auth(Resource):
    @staticmethod
    def post():
        payload = login_parser.parse_args()
        email = payload[EMAIL]
        password = payload[PWD]

        if not email:
            return {MSG: FIELD_REQUIRED.format(EMAIL)}, 400
        if not password:
            return {MSG: FIELD_REQUIRED.format(PWD)}, 400

        try:
            target = get_user(email)
        except APIException as error:
            return {MSG: error.message}, error.http_code

        else:
            if target and not target.oauth and Encrypt.verify(password, target.password):
                access_token = create_access_token(identity=target.uid, fresh=True)
                refresh_token = create_refresh_token(identity=target.uid)
                return (
                    {TOKEN: access_token, R_TOKEN: refresh_token},
                    200,
                )

        return {MSG: BAD_CRED}, 401

    @staticmethod
    @jwt_refresh_token_required
    def put():
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user, fresh=True)
        refresh_token = create_refresh_token(identity=current_user)
        return {TOKEN: access_token, R_TOKEN: refresh_token}, 200

from flask_restful import Resource, reqparse
from beluuga.constants.keys import MSG
from beluuga.constants.errors import FIELD_REQUIRED

from flask_jwt_extended import get_jwt_identity, jwt_required

from beluuga.service.user import create_user
from beluuga.service.user import get_user_by_id
from beluuga.constants.columns import EMAIL, PWD, F_NAME, L_NAME, PHONE
from beluuga.exceptions.api import APIException

user_parser = reqparse.RequestParser()
user_parser.add_argument(
    EMAIL, type=str, required=True, help=FIELD_REQUIRED.format(EMAIL)
)
user_parser.add_argument(PWD, type=str, required=True, help=FIELD_REQUIRED.format(PWD))
user_parser.add_argument(
    F_NAME, type=str, required=True, help=FIELD_REQUIRED.format(F_NAME)
)
user_parser.add_argument(
    L_NAME, type=str, required=True, help=FIELD_REQUIRED.format(L_NAME)
)
user_parser.add_argument(
    PHONE, type=str, required=False, help=FIELD_REQUIRED.format(PHONE)
)


class UserCreate(Resource):
    @staticmethod
    def post():
        data = user_parser.parse_args()
        try:
            new_user = create_user(data)
        except APIException as error:
            return {MSG: error.message}, error.http_code

        return new_user.json(), 201

    @staticmethod
    @jwt_required
    def get():
        identity = get_jwt_identity()
        current_user = get_user_by_id(identity)
        if current_user:
            return current_user.json(), 200
        else:
            return {MSG: "Something went wrong!"}, 500

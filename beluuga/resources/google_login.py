from flask_restful import Resource, reqparse
from beluuga.service.oauth2 import OAuth2
from beluuga.constants.errors import FIELD_REQUIRED
from beluuga.constants.keys import MSG, TOKEN, R_TOKEN
from beluuga.service.user import get_user
from beluuga.service.user import create_user
from config import get_config_for_env

from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
)

google_parser = reqparse.RequestParser()
google_parser.add_argument('clientId', type=str, required=True, help=FIELD_REQUIRED.format('clientId'))
google_parser.add_argument('redirectUri', type=str, required=True, help=FIELD_REQUIRED.format('redirectUri'))
google_parser.add_argument('code', type=str, required=True, help=FIELD_REQUIRED.format('code'))
cfg = get_config_for_env()


class GoogleAuthorized(Resource):

    @staticmethod
    def post():
        req = google_parser.parse_args()
        client_code = req.get('code')
        redirect_uri = req.get('redirectUri')
        google = OAuth2.get_provider('google', cfg.GOOGLE_CLIENT_ID)

        try:
            res = google.get_resources(
                client_id=cfg.GOOGLE_CLIENT_ID,
                client_code=client_code,
                redirect_uri=redirect_uri,
                client_secret=cfg.GOOGLE_CLIENT_SECRET)
        except TypeError as error:
            return {MSG: f"Could not authenticate the Google account! : {error}"}, 401

        user = get_user(res['email'])
        if not user:
            new_user = {
                "email": res['email'],
                "f_name": res['given_name'],
                "l_name": res['family_name'],
                "password": None,
                "phone_number": None,
                "oauth": True
            }
            user = create_user(new_user, oauth=True)

        access_token = create_access_token(identity=user.uid, fresh=True)
        refresh_token = create_refresh_token(identity=user.uid)
        return {TOKEN: access_token, R_TOKEN: refresh_token}, 200

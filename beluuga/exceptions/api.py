class APIException(Exception):
    def __init__(self, http_code: int, message: str = "", cause: Exception = None):
        self.http_code = http_code
        self.message = message
        self.cause = cause

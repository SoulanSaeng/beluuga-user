import os
import requests
from requests import Response
import logging
from beluuga.models.user import UserModel

LOGGER = logging.getLogger("gunicorn.error")


def get_redirect_url() -> str:
    return os.getenv("RESET_URL")


class Mailer(object):
    def __init__(self, api_key: str, api_base_url: str):
        self.api_key = api_key
        self.api_base_url = api_base_url

    def send_reset_pwd(self, sender: str, recipient: UserModel, secret: str, subject: str) -> Response:
        auth = ("api", self.api_key)
        magic_url = get_redirect_url() + "email=" + recipient.email + "&" + "otp=" + secret
        html = f"Hi there, follow the link to reset your password: {magic_url}"
        data = {"from": sender, "to": [recipient.email], "subject": subject, "text": html}

        return requests.post(self.api_base_url, auth=auth, data=data)

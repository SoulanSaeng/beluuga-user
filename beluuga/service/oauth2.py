from __future__ import annotations
from abc import ABCMeta, abstractmethod
from typing import Dict
from oauth2client.client import OAuth2WebServerFlow
from googleapiclient.discovery import build
import httplib2

GOOGLE_SCOPE = 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
FACEBOOK_SCOPE = ''


class OAuth2(metaclass=ABCMeta):

    def __init__(self, client_id: str):
        self.client_id = client_id

    @abstractmethod
    def get_resources(self, **kwargs) -> Dict:
        pass

    @staticmethod
    def get_provider(provider: str, client_id: str) -> OAuth2:
        if provider == 'google':
            return GoogleOAuth2(client_id)


class GoogleOAuth2(OAuth2):

    def get_resources(self, **kwargs) -> Dict:

        client_id = kwargs.get('client_id')
        if self.client_id != client_id:
            raise TypeError('application client_id and provided client_id mismatch.')

        client_code = kwargs.get('client_code')
        client_secret = kwargs.get('client_secret')
        redirect_uri = kwargs.get('redirect_uri')

        if client_id is None:
            raise TypeError('client_id must be defined!')
        elif client_code is None:
            raise TypeError('client_code must be defined!')
        if client_secret is None:
            raise TypeError('client_secret must be defined!')
        elif redirect_uri is None:
            raise TypeError('redirect_uri must be defined!')

        flow = OAuth2WebServerFlow(client_id=client_id,
                                   client_secret=client_secret,
                                   scope=GOOGLE_SCOPE,
                                   redirect_uri=redirect_uri)

        oauth_2 = flow.step2_exchange(code=client_code)
        service = build('oauth2', 'v2', credentials=oauth_2)
        http = httplib2.Http()
        http = oauth_2.authorize(http)
        user_details = service.userinfo().get().execute(http=http)

        return user_details


"""
    TODO implement Facebook OAuth login.
"""


class FacebookOAuth2(OAuth2):

    def get_resources(self, **kwargs) -> Dict:
        pass

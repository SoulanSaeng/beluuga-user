import logging

from flask import g
from pymysql import OperationalError

from sqlalchemy.exc import SQLAlchemyError
from werkzeug.local import LocalProxy

from beluuga.exceptions.api import APIException
from beluuga.constants.columns import EMAIL, PWD, F_NAME, L_NAME
from beluuga.security.validator import PasswordValidator
from beluuga.security.encrypt import Encrypt

from beluuga.constants.errors import RESOURCE_EXISTS, BAD_PWD, BAD_F_NAME, BAD_L_NAME
from beluuga.models.user import UserModel
from beluuga.models.otp import OtpModel
from beluuga.service.messaging import Mailer

from config import get_config_for_env


LOGGER = logging.getLogger("gunicorn.error")
cfg = get_config_for_env()


def get_user(user_email: str) -> UserModel:
    try:
        current_user = UserModel.find_by_identity(user_email)

    except OperationalError as err:
        LOGGER.warning("Broken pipe error, trying again...", err)
        return UserModel.find_by_identity(user_email)

    except AttributeError as err:
        LOGGER.error("Database schema inconsistent with the model definition!", err)
        raise APIException(http_code=500, cause=err)
    except Exception as err:
        LOGGER.error("Severe error occurred!", err)
        raise APIException(http_code=500, cause=err)
    else:
        return current_user


def get_user_by_id(uid: int) -> UserModel:
    try:
        current_user = UserModel.find_by_uid(uid)

    except OperationalError as err:
        LOGGER.warning("Broken pipe error, trying again...", err)
        return UserModel.find_by_uid(uid)
    except AttributeError as err:
        LOGGER.error("Database schema inconsistent with the model definition!", err)
        raise APIException(http_code=500, cause=err)
    except Exception as err:
        LOGGER.error("Severe error occurred!", err)
        raise APIException(http_code=500, cause=err)
    else:
        return current_user


def get_mailer():
    if 'mailer' not in g:
        g.mailer = Mailer(api_key=cfg.MAILGUN_API_KEY,
                          api_base_url=cfg.MAILGUN_API_URL)
        return g.mailer


def send_reset_pw_mail(user_email: str):
    current_user = get_user(user_email)
    if not current_user or current_user.oauth:
        raise APIException(http_code=404, message="Email does not exist.")

    secret = Encrypt.otp(length=16, special_char=True)
    mailer = LocalProxy(get_mailer)
    response = mailer.send_reset_pwd(
        "beluuga-accounts@beluuga.io",
        current_user,
        secret,
        "Your password reset request.",
    )
    if response.status_code == 200:
        if current_user.otp:
            current_user.otp.update_secret(secret)
            current_user.persist()
        else:
            current_user.otp = OtpModel(current_user.uid, secret)
            current_user.persist()
        return True
    else:
        LOGGER.error("Unable to send mail, check if the credentials and the URL are correct.")
        raise APIException(http_code=500, message="oh uh, we're having problems with our emails.")


def create_user(data: dict, oauth: bool = False) -> UserModel:
    try:
        db_user = get_user(data[EMAIL])
    except Exception as err:
        raise err

    else:
        if db_user:
            LOGGER.warning("User %s already exists.", data[EMAIL])
            raise APIException(400, RESOURCE_EXISTS.format(data[EMAIL]), None)

        else:
            # oauth users do not need to provide a password.
            if not oauth and not PasswordValidator.validate_pwd(data[PWD]):
                LOGGER.error("Bad password provided.")
                raise APIException(http_code=400, message=BAD_PWD)

            elif len(data[F_NAME].strip()) == 0:
                raise APIException(http_code=400, message=BAD_F_NAME)

            elif len(data[L_NAME].strip()) == 0:
                raise APIException(http_code=400, message=BAD_L_NAME)

            """ Create the user. """
            new_user = UserModel(**data)
            try:
                new_user.persist()
            except SQLAlchemyError as err:
                LOGGER.error(err)
                LOGGER.error("Error occurred while trying to save the user.")
                raise APIException(
                    http_code=500,
                    message="Error occurred while trying to save the user.",
                    cause=err,
                )

            return new_user


def update_pwd(email: str, otp: str, pwd: str) -> bool:
    current_user = get_user(email)
    current_otp = OtpModel.find_by_uid(current_user.uid)
    LOGGER.info(current_otp.uid)
    if current_otp and Encrypt.verify(otp, current_otp.secret):
        current_user = get_user(email)
        current_user.update_pwd(pwd)
        current_user.persist()
        current_otp.delete()
        return True

    LOGGER.warning("Bad OTP or Email does not exist, cannot change pwd.")
    return False

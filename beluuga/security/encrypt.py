import bcrypt
from secrets import token_hex, token_urlsafe
from typing import Tuple


DEFAULT_ENCODING = "utf-8"


class Encrypt(object):
    @staticmethod
    def hash(secret: str) -> Tuple[str, str]:
        salt = bcrypt.gensalt()
        hashed = bcrypt.hashpw(secret.encode(DEFAULT_ENCODING), salt)

        return hashed, salt

    @staticmethod
    def verify(pwd: str, hashed: str) -> bool:
        if not pwd or not hashed:
            return False

        return bcrypt.checkpw(
            pwd.encode(DEFAULT_ENCODING), hashed.encode(DEFAULT_ENCODING)
        )

    @staticmethod
    def otp(length: int = 16, special_char: bool = True) -> str:
        if not special_char:
            return token_hex(length)
        else:
            return token_urlsafe(length)

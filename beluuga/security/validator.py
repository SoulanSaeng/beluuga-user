class PasswordValidator(object):

    """
        Given a password, ensure the password is secure enough.
        Password must contain at least one numerical character.
        Password must be of length > 8
        Password must contain at least one capital character.
    """

    @staticmethod
    def validate_pwd(pwd: str) -> bool:
        if len(pwd) < 8:
            return False

        has_capital = False
        has_numerical = False
        for c in pwd:
            if c.isupper():
                has_capital = True
            elif c.isdigit():
                has_numerical = True

        return has_capital and has_numerical

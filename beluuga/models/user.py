from __future__ import annotations
from typing import Dict, Union
from beluuga.persist import db
from beluuga.constants.columns import EMAIL, F_NAME, L_NAME, USER_TABLE, PHONE
from beluuga.security.encrypt import Encrypt

UserJSON = Dict[str, Union[str, int]]


class UserModel(db.Model):
    __tablename__ = USER_TABLE

    uid = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(80), nullable=True)
    salt = db.Column(db.String(80), nullable=True)
    phone_number = db.Column(db.String(80), nullable=True, unique=True)
    f_name = db.Column(db.String(45))
    l_name = db.Column(db.String(45))
    oauth = db.Column(db.Boolean, default=False)
    otp = db.relationship("OtpModel", uselist=False, back_populates="user")

    def __init__(
        self, email: str, password: str, phone_number: str, f_name: str, l_name: str, oauth: bool = False
    ):
        self.email = email

        if password is not None:
            self.password, self.salt = Encrypt.hash(password)

        self.phone_number = phone_number
        self.f_name = f_name
        self.l_name = l_name
        self.oauth = oauth

    def json(self) -> UserJSON:
        return {
            EMAIL: self.email,
            PHONE: self.phone_number,
            F_NAME: self.f_name,
            L_NAME: self.l_name,
        }

    def update_pwd(self, password):
        self.password, self.salt = Encrypt.hash(password)

    @classmethod
    def find_by_identity(cls, _identity: str) -> UserModel:
        return cls.query.filter(
            (UserModel.email == _identity) | (UserModel.phone_number == _identity)
        ).first()

    @classmethod
    def find_by_email(cls, _email: str) -> UserModel:
        return cls.query.filter_by(email=_email).first()

    @classmethod
    def find_by_phone(cls, _phone: str) -> UserModel:
        return cls.query.filter_by(phone=_phone).first()

    @classmethod
    def find_by_uid(cls, _uid: int) -> UserModel:
        return cls.query.filter_by(uid=_uid).first()

    def persist(self) -> None:
        db.session.add(self)
        db.session.commit()

    def delete(self) -> None:
        db.session.delete(self)
        db.session.commit()

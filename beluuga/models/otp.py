from __future__ import annotations
from beluuga.persist import db
from beluuga.constants.columns import OTP_TABLE
from beluuga.security.encrypt import Encrypt


class OtpModel(db.Model):

    __tablename__ = OTP_TABLE
    otpid = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.Integer, db.ForeignKey('users.uid'), nullable=False, unique=True)
    user = db.relationship("UserModel", back_populates="otp")
    secret = db.Column(db.String(80), nullable=True)
    salt = db.Column(db.String(80), nullable=True)

    def __init__(self, uid: str, otp: str):
        self.uid = uid
        self.secret, self.salt = Encrypt.hash(otp)

    def update_secret(self, secret: str):
        self.secret, self.salt = Encrypt.hash(secret)

    @classmethod
    def find_by_uid(cls, _uid: int) -> OtpModel:
        return cls.query.filter_by(uid=_uid).first()

    @classmethod
    def find_by_optid(cls, _optid: int) -> OtpModel:
        return cls.query.filter_by(optid=_optid).first()

    def persist(self) -> None:
        db.session.add(self)
        db.session.commit()

    def delete(self) -> None:
        db.session.delete(self)
        db.session.commit()

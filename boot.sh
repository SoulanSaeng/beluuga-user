#!/usr/bin/env bash
source venv/bin/activate
exec gunicorn -b :5000 --log-level debug --error-logfile -  --timeout 120 --worker-class gevent  app:app

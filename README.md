# USER API

## Deploy

With Docker & Docker-Compose installed on your machine `./build-deploy.sh`

## Authentication Endpoints
A following up the authentication call, the service will respond with
an authentication token and a refresh token.

### `HTTP POST /auth` Authenticate a user
##### Request Body
```js
    {
      "email" : "JohnDoe@gmail.com",
      "password" : "ABC1234"
    }
 ```
##### Response Body
```js
    {
      "token" : "SOMEJWTTOKEN",
      "refresh_token" : "SOMEREFRESHTOKEN",
    }
 ```
 
 ### `HTTP PUT /auth` Refreshes token 
 
```js
    {
      "token" : "SOMEJWTTOKEN",
      "refresh_token" : "SOMEREFRESHTOKEN",
    }
 ```
## User Endpoints

### `HTTP POST /user` Register a user
##### Request Body
```js
    {
      "email" : "JohnDoe@gmail.com",
      "f_name" : "John",
      "l_name" : "Doe",
      "phone_number" : "51499999",
      "password" : "ABC1234"
    }
 ```
##### Response Body
 `HTTP 201 CREATED`
```js
{
    "f_name" : "John",
    "l_name" : "Doe",
    "phone" : "5148889999",
    "email" : "Jd@gmail.com"
 }
 ```
 
### `HTTP PUT /user/password` Reset password
##### Request Body
```js
    {
      "email" : "JohnDoe@gmail.com",
    }
 ```
 ### `HTTP POST /user/password` Confirms new password
##### Request Body
```js
    {
      "email" : "JohnDoe@gmail.com",
      "one_time_password": "ONETIMETOKEN",
      "new_password" : "mynewpassworD1"
    }
 ```
 ##### Response Body
 `HTTP 200 OK`
```js
{
    "message": "A reset link was sent to the email."
}
 ```
### `HTTP GET /user` Retrieves user info

##### Response Body
```js
{
    "f_name" : "John",
    "l_name" : "Doe",
    "phone" : "5148889999",
    "email" : "JohnDoe@gmail.com"
 }
 ```
 ## HTTP Error Codes
- HTTP 400 : Something is wrong with your request. 
- HTTP 401 : Bad username and password combination.
- HTTP 500 : Something went wrong on our end. 

 
 ## Environment Variables
- JWT_ALGORITHM `JWT Signing Algorithms, defaults to HS256`
- JWT_SECRET_KEY `Secret to use when creating token`
- JWT_SECRET_TOKEN_EXPIRES `TTL before token expires`
- DATABASE_HOST `MySQL database host, defaults to localhost`
- DATABASE_PORT `MySQL database port, defaults to 3306`
- DATABASE_USER `MySQL database port, defaults to beluuga`
- DATABASE_PASSWORD `MySQL database password`
- DATABASE_SCHEMA `MySQL database name`
- DATABASE_PROTOCOL `Database dialect and DB API to use. See https://docs.sqlalchemy.org/en/13/core/engines.html for more details.
 defaults to mysql+pymysql`
- MAILGUN_API_KEY `MailGun API key used for account confirmation and password reset.`
- MAILGUN_API_URL `The MailGun API endpoint.`
- RESET_URL `The password reset URI to pass along within the email.`
- FLASK_ENV `The flask environment type : development, production or testing. Defaults to development`
- FLASK_APP `Tells flask which python file is the main runner.`
- GOOGLE_CLIENT_ID `The client id from Google APIs, used for OAuth2 based authentication.`
- GOOGLE_CLIENT_SECRET `The client secret from Google APIs, used for OAuth2 based authentication.`
- GOOGLE_REDIRECT_URI `The redirect URI for OAuth2`
- FACEBOOK_CLIENT_ID `Facebook client id for OAuth2`
- FACEBOOK_CLIENT_SECRET `Facebook client secret for OAuth2`
- FACEBOOK_REDIRECT_URI `The redirect URI for OAuth2`
 

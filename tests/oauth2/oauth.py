import unittest
from beluuga.service.oauth2 import OAuth2
from config import get_config_for_env

cfg = get_config_for_env()


class TestOAuth2(unittest.TestCase):

    def test_get_res(self):
        google = OAuth2.provider('google', cfg.GOOGLE_CLIENT_ID)
        res = google.get_resources(
            client_id=cfg.GOOGLE_CLIENT_ID,
            client_code='',
            redirect_uri='http://localhost:8080/',
            client_secret=cfg.GOOGLE_CLIENT_SECRET
        )

        self.assertIsNotNone(res)

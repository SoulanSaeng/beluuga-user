import unittest
from beluuga.security.validator import PasswordValidator

SHORT = "blah"
MISSING_NUMERICAL = "BlahBlah"
MISSING_CAPITAL = "blablabla"
BLANK = ""
NUMERICAL_ONLY = "91591581"
SECURE_PWD = "Blah1959halB"


class TestPasswordValidation(unittest.TestCase):

    def test_invalid_length(self):
        self.assertEquals(False, PasswordValidator.validate_pwd(SHORT))

    def test_missing_numerical(self):
        self.assertEquals(False, PasswordValidator.validate_pwd(MISSING_NUMERICAL))

    def test_missing_capital(self):
        self.assertEquals(False, PasswordValidator.validate_pwd(MISSING_NUMERICAL))

    def test_blank(self):
        self.assertEquals(False, PasswordValidator.validate_pwd(BLANK))

    def test_numerical_only(self):
        self.assertEquals(False, PasswordValidator.validate_pwd(NUMERICAL_ONLY))

    def secure_pwd(self):
        self.assertEquals(True, PasswordValidator.validate_pwd(SECURE_PWD))


if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env bash
docker-compose down
sleep 5
docker build . --tag beluuga-user:latest
docker-compose up -d
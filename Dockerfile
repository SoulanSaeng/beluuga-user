FROM python:3.7.2-slim-stretch
RUN useradd beluuga

WORKDIR /opt/beluuga
COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY app.py app.py
COPY config.py config.py
COPY instance instance
COPY beluuga beluuga
COPY boot.sh boot.sh

RUN chmod +x boot.sh
RUN chown -R beluuga:beluuga ./

ENV FLASK_ENV development
ENV FLASK_APP app
USER beluuga
EXPOSE 5000

ENTRYPOINT [ "./boot.sh" ]

